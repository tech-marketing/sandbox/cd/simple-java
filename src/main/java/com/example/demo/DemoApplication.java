package com.example.demo;

import org.springframework.boot.*;
import org.springframework.boot.autoconfigure.*;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

@SpringBootApplication
@RestController
public class DemoApplication {

	String style = "<style type='text/css' media='screen'>" + "body { background-color: #c4b5eeff; position: fixed; top: 50%; left: 50%; transform: translate(-50%, -50%); color: white; font-size: 250%; }" + "</style>";

	@GetMapping("/")
	String home() {
        // TODO - Personalize this message!
        String message = "Simple Java is here!";
        System.out.println("handling GET");        
        String body =
                "<body>" +
                message +
                "<br>" +
                "<form action=\"/\" method=\"post\" >" +
				"<input type=\"submit\" value=\"Generate 500 error\">" +
				"</form>" +
                "</body>";

        return style + body;
	}

	@PostMapping("/")
	public @ResponseBody String home(String response) {

		String body = response.trim();
		return body;
	}

	public static void main(String[] args) {
		SpringApplication.run(DemoApplication.class, args);
	}
}
